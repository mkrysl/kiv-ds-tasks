use std::time::Duration;

use reqwest::Url;

use crate::conf::NodeConfig;
use crate::message::{UpdateRequest, UpdateResponse, VoteRequest, VoteResponse};
use crate::timing::REQUEST_TIMEOUT;

/// RAFT HTTP client for sending API requests to other nodes.
pub struct Client {
    client: reqwest::Client,
}


impl Client {
    /// Create new RAFT HTTP client.
    pub fn new() -> Self {
        let client = reqwest::ClientBuilder::new()
            .timeout(Duration::from_millis(REQUEST_TIMEOUT))
            .build().unwrap();

        Client {
            client
        }
    }

    /// Send vote request to a node.
    pub async fn vote(&self, node: NodeConfig, vote_request: VoteRequest) -> Option<VoteResponse> {

        // POST /vote
        let url = format!("http://{}:{}/{}", &node.host, node.port, "vote");
        let url = Url::parse(&url).unwrap();

        // send request
        let response = self.client
            .post(url)
            .json(&vote_request)
            .send().await;

        match response {
            Ok(response) => {
                // parse json
                let response = response.json()
                    .await
                    .unwrap();

                Some(response)
            }
            Err(_) => None
        }
    }

    /// Send update request to a node.
    pub async fn update(&self, node: NodeConfig, update: UpdateRequest) -> Option<UpdateResponse> {

        // POST /update
        let url = format!("http://{}:{}/{}", &node.host, node.port, "update");
        let url = Url::parse(&url).unwrap();

        // send request
        let response = self.client
            .post(url)
            .json(&update)
            .send().await;

        match response {
            Ok(response) => {
                // parse json
                let response = response.json()
                    .await
                    .unwrap();

                Some(response)
            }
            Err(_) => None
        }
    }
}