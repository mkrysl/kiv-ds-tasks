use std::convert::Infallible;
use std::net::SocketAddr;
use warp::Filter;

use crate::app::AppQueueSender;
use crate::message::{UpdateRequest, VoteRequest};

/// Run nodes HTTP server on given address.
/// 
/// Events are created for every request from client and passed to the app queue
/// together with a channel for response.
/// 
/// Once the app queue handles the event and sends a response, the HTTP server responds
/// to the client.
pub async fn run_http_server<>(address: SocketAddr, app: AppQueueSender) {

    // to inject app queue sender into route closures
    let with_app = |app: AppQueueSender| {
        warp::any().map(move || app.clone())
    };

    // POST /vote
    let vote_route = warp::path("vote")
        .and(warp::post())
        .and(with_app(app.clone()))
        .and(warp::body::json())
        .and_then(vote);

    // POST /update
    let update_route = warp::path("update")
        .and(warp::post())
        .and(with_app(app.clone()))
        .and(warp::body::json())
        .and_then(update);

    warp::serve(
        vote_route
            .or(update_route)
    )
        .run(address)
        .await;
}

/// Vote request.
pub async fn vote(app: AppQueueSender, vote_request: VoteRequest) -> Result<impl warp::Reply, Infallible> {
    let response = app.vote(vote_request).await;
    Ok(warp::reply::json(&response))
}


/// Update request
pub async fn update(app: AppQueueSender, update: UpdateRequest) -> Result<impl warp::Reply, Infallible> {
    let response = app.update(update).await;
    Ok(warp::reply::json(&response))
}