use std::collections::BTreeMap;
use serde::{Deserialize};


/// Application config.
#[derive(Clone, Debug)]
pub struct Config {
    pub id: String,
    pub nodes: BTreeMap<String, NodeConfig>,
}


impl Config {
    /// Get this node's config.
    pub fn this_node(&self) -> &NodeConfig {
        &self.nodes[&self.id]
    }

    /// Get this a node's config by node ID.
    pub fn node(&self, id: &str) -> &NodeConfig {
        &self.nodes[id]
    }

    /// Get config of all other nodes except this node.
    pub fn other_nodes(&self) -> impl Iterator<Item = &NodeConfig> {
        self.nodes.iter().filter_map(|(id, node)| {
            if id == &self.id {
                None
            } else {
                Some(node)
            }
        })
    }

    /// Get the count of all other nodes except this node.
    pub fn other_nodes_count(&self) -> usize {
        self.nodes.len() - 1
    }
}


/// Struct for deserializing config file.
#[derive(Deserialize)]
pub struct ConfigFile {
    pub nodes: Vec<NodeConfig>,
}


/// Configuration of a single node.
#[derive(Deserialize, Clone, Debug)]
pub struct NodeConfig {
    pub id: String,
    pub host: String,
    pub port: u16,
}


/// Default path to configuration file.
const CONFIG_PATH: &str = "conf/config.toml";


/// Parse the configuration.
///
/// It takes the node ID from environment variable `ID` and nodes configurations
/// from file on relative path `conf/config.toml`
pub fn parse_config() -> Config {
    let config_string = std::fs::read_to_string(CONFIG_PATH).expect("unable to read config file");
    let config: ConfigFile = toml::from_str(&config_string).expect("error while parsing the config file");

    let nodes_map = config.nodes.into_iter()
        .map(|n| (n.id.clone(), n)).collect::<BTreeMap<_, _>>();

    let id = std::env::var("ID")
        .expect("Node `ID` environment variable is not set.");


    if !nodes_map.contains_key(&id) {
        panic!("This node's ID is not in the configuration file.");
    }

    Config {
        id,
        nodes: nodes_map,
    }
}