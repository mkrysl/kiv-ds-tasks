use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UpdateRequest {
    pub term: usize,
    pub color: Option<ColorUpdate>,
}


impl UpdateRequest {
    pub fn empty(term: usize) -> Self {
        UpdateRequest {
            term,
            color: None,
        }
    }

    pub fn color(term: usize, color: ColorUpdate) -> Self {
        UpdateRequest {
            term,
            color: Some(color),
        }
    }
}


#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ColorUpdate {
    Red,
    Green,
}


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UpdateResponse {
    pub ack: bool,
}


impl UpdateResponse {
    pub fn new(ack: bool) -> Self {
        UpdateResponse {
            ack
        }
    }
}


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct VoteRequest {
    pub term: usize,
}

impl VoteRequest {
    pub fn new(term: usize) -> Self {
        VoteRequest {
            term
        }
    }
}


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct VoteResponse {
    pub voted: bool,
}

impl VoteResponse {
    pub fn new(voted: bool) -> Self {
        VoteResponse {
            voted
        }
    }
}
