use std::net::SocketAddr;

use env_logger::{Builder, Target};
use log::LevelFilter;

use crate::app::App;
use crate::conf::parse_config;
use crate::server::run_http_server;

mod conf;
mod server;
mod message;
mod client;
mod timing;
mod app;
mod state;


#[tokio::main]
async fn main() {
    // init logger
    Builder::from_default_env()
        .target(Target::Stdout)
        .filter(Some("node"), LevelFilter::Info)
        .init();

    // init config
    let config = parse_config();
    let node_config = config.this_node().clone();

    // init app
    let mut app = App::new(config);
    let queue_sender = app.queue_sender();

    // run app in other task
    tokio::spawn(async move { app.run().await });

    // init and start http server
    let address = SocketAddr::from(([0, 0, 0, 0], node_config.port));
    run_http_server(address, queue_sender).await;
}
