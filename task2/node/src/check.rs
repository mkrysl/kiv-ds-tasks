use std::process::exit;

/// Program to check if the node is running correctly.
fn main() {
    let port: u16 = std::env::args().into_iter().nth(1)
        .expect("Node `PORT` argument is not set.")
        .parse()
        .expect("Invalid port number.");

    // send check request to node and wait for the result
    let result = reqwest::blocking::get(format!("http://localhost:{}/check", port));

    if result.is_err() {
        exit(1);
    }
}
