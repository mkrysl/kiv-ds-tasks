use std::fmt::{Display, Formatter};

use log::info;

/// Application state.
pub struct AppState {
    state: State,
    term: usize,
    followers: Vec<String>,
    color: Color,
}


impl AppState {
    pub fn new() -> Self {
        AppState {
            state: State::Follower,
            term: 0,
            followers: Vec::new(),
            color: Color::None,
        }
    }

    /// Vote in the given term if the term is greater than current, and updates the current
    /// election term.
    /// Returns true if the given term is greater and the node votes for.
    pub fn vote(&mut self, term: usize) -> bool {
        if term > self.term {
            self.term = term;
            self.update_state(State::Follower);
            true
        } else {
            false
        }
    }

    /// Updates term with the new value if the value is greater.
    /// Returns true if the given term is greater or equal than the current one.
    pub fn update_term(&mut self, term: usize) -> bool {
        if term >= self.term {
            self.term = term;
            self.update_state(State::Follower);
            true
        } else {
            false
        }
    }

    /// Update node color.
    pub fn update_color(&mut self, color: Color) {
        if self.color != color {
            self.color = color;

            info!("color changed to: {}", self.color);
        }
    }

    /// Update list of followers.
    /// Returns true if the list is different from the old one.
    pub fn update_followers(&mut self, followers: Vec<String>) -> bool {
        if self.followers != followers {
            self.followers = followers;
            info!("followers change: [{}]", self.followers.join(", "));
            true
        } else {
            false
        }
    }

    /// Update the node state.
    pub fn update_state(&mut self, state: State) {
        if self.state != state {
            self.state = state;

            info!("state changed to: {}", self.state);

            match self.state {
                State::Follower => {}
                State::Candidate => {
                    self.term += 1;
                }
                State::Leader => {}
            }
        }
    }

    /// Get the node state.
    pub fn state(&self) -> &State {
        &self.state
    }

    /// Get the node term.
    pub fn term(&self) -> usize {
        self.term
    }

    /// Get the node followers.
    pub fn followers(&self) -> &Vec<String> {
        &self.followers
    }
}


/// Node color.
#[derive(Eq, PartialEq)]
pub enum Color {
    Red,
    Green,
    None,
}


impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let color = match self {
            Color::Red => "RED",
            Color::Green => "GREEN",
            Color::None => "NONE"
        };

        write!(f, "{}", color)
    }
}


/// Node state.
#[derive(Eq, PartialEq)]
pub enum State {
    Leader,
    Follower,
    Candidate,
}


impl Display for State {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let state = match self {
            State::Leader => "LEADER",
            State::Follower => "FOLLOWER",
            State::Candidate => "CANDIDATE"
        };

        write!(f, "{}", state)
    }
}