use std::iter;
use std::sync::Arc;
use std::time::Duration;

use futures::future::join_all;
use log::info;
use rand::Rng;
use tokio::sync::{mpsc, oneshot};
use tokio::sync::oneshot::Sender;

use crate::client::Client;
use crate::conf::{Config, NodeConfig};
use crate::message::{ColorUpdate, UpdateRequest, UpdateResponse, VoteRequest, VoteResponse};
use crate::state::{AppState, Color, State};
use crate::timing::{ELECTION_TIMEOUT_MAX, ELECTION_TIMEOUT_MIN, HEARTBEAT_TIMEOUT};

/// App event that is handled inside an app event queue.
pub enum AppEvent {
    Vote(VoteRequest, oneshot::Sender<VoteResponse>),
    Update(UpdateRequest, oneshot::Sender<UpdateResponse>),
    VotingResult(VotingResult),
    UpdateResult(UpdateResult),
    Check(oneshot::Sender<()>)
}


/// Voting result.
pub struct VotingResult {
    pub responses: Vec<Option<VoteResponse>>,
}


impl VotingResult {
    pub fn new(responses: Vec<Option<VoteResponse>>) -> Self {
        VotingResult {
            responses
        }
    }
}


/// Update result.
pub struct UpdateResult {
    pub responses: Vec<Option<UpdateResponse>>,
}


impl UpdateResult {
    pub fn new(responses: Vec<Option<UpdateResponse>>) -> Self {
        UpdateResult {
            responses
        }
    }
}


/// Nodes main application logic.
pub struct App {
    app_state: AppState,
    client: Arc<Client>,
    config: Config,
    event_sender: mpsc::Sender<AppEvent>,
    event_receiver: mpsc::Receiver<AppEvent>,
}


impl App {
    /// Create a new application.
    pub fn new(config: Config) -> Self {
        let (sender, receiver) = mpsc::channel(128);

        App {
            app_state: AppState::new(),
            client: Arc::new(Client::new()),
            config,
            event_sender: sender,
            event_receiver: receiver,
        }
    }

    /// Get this app event queue sender to interact with the app.
    pub fn queue_sender(&self) -> AppQueueSender {
        AppQueueSender {
            sender: self.event_sender.clone()
        }
    }

    /// Run the application event queue.
    pub async fn run(&mut self) {
        info!("ID: {}", self.config.id);
        info!("starting in state: {}", self.app_state.state());

        // setup timeouts
        let timeout_heart_beat = Duration::from_millis(HEARTBEAT_TIMEOUT);
        let timeout_election = || {
            Duration::from_millis(rand::thread_rng().gen_range(
                ELECTION_TIMEOUT_MIN..ELECTION_TIMEOUT_MAX
            ))
        };

        loop {
            // select timeout based on the current node state
            let timeout = match self.app_state.state() {
                State::Leader => timeout_heart_beat,
                State::Follower => timeout_election(),
                State::Candidate => Duration::MAX
            };

            // wait for event or timeout
            let result = tokio::time::timeout(
                timeout,
                self.event_receiver.recv()).await;

            match result {
                Ok(event) => self.handle_event(event).await,
                Err(_) => self.handle_timeout().await,
            }
        }
    }

    /// Handle app event.
    async fn handle_event(&mut self, event: Option<AppEvent>) {
        let event = event.unwrap();

        match event {
            AppEvent::Vote(request, responder) => self.handle_vote_event(request, responder).await,
            AppEvent::Update(request, responder) => self.handle_update_event(request, responder).await,
            AppEvent::VotingResult(result) => self.handle_voting_result_event(result).await,
            AppEvent::UpdateResult(result) => self.handle_update_result_event(result).await,
            AppEvent::Check(responder) => self.handle_check_event(responder).await,
        }
    }

    /// Handle incoming vote event.
    pub async fn handle_vote_event(&mut self, request: VoteRequest, responder: Sender<VoteResponse>) {
        let voted = self.app_state.vote(request.term);
        responder.send(VoteResponse::new(voted)).unwrap();
    }

    /// Handle incoming update event.
    pub async fn handle_update_event(&mut self, request: UpdateRequest, responder: Sender<UpdateResponse>) {
        let is_follower = self.app_state.update_term(request.term);

        if is_follower {
            // update node's color
            if let Some(color) = request.color {
                match color {
                    ColorUpdate::Red => self.app_state.update_color(Color::Red),
                    ColorUpdate::Green => self.app_state.update_color(Color::Green)
                }
            }
        }

        responder.send(UpdateResponse::new(is_follower)).unwrap();
    }

    /// Handle incoming check event.
    pub async fn handle_check_event(&mut self, responder: Sender<()>) {
        responder.send(()).unwrap();
    }

    /// Handle voting result event.
    pub async fn handle_voting_result_event(&mut self, result: VotingResult) {
        if self.app_state.state() != &State::Candidate {
            return;
        }

        let mut voted = 0;
        let mut responded = 0;

        for vote in result.responses.into_iter().flatten() {
            if vote.voted {
                voted += 1;
            }
            responded += 1;
        }

        if voted >= responded / 2 {
            self.app_state.update_state(State::Leader);
            self.run_colorization().await;
        } else {
            self.app_state.update_state(State::Follower);
        }
    }

    /// Handle update result event.
    pub async fn handle_update_result_event(&mut self, result: UpdateResult) {
        if self.app_state.state() != &State::Leader {
            return;
        }

        if result.responses.len() != self.config.other_nodes_count() {
            return;
        }

        let followers = self.config
            .other_nodes()
            .zip(result.responses)
            .filter_map(|(node, response)|
                response.map(|_| node.id.clone()))
            .collect();

        let changed = self.app_state.update_followers(followers);

        if changed {
            self.run_colorization().await;
        }
    }

    /// Handle timeout.
    async fn handle_timeout(&mut self) {
        match self.app_state.state() {
            State::Leader => self.handle_leader_timeout().await,
            State::Follower => self.handle_follower_timeout().await,
            State::Candidate => unreachable!("no timeout should happen in the CANDIDATE state")
        }
    }

    /// Handle leader timeout.
    async fn handle_leader_timeout(&mut self) {
        let nodes = self.config.other_nodes();
        let update = UpdateRequest::empty(self.app_state.term());

        let updates = nodes.zip(iter::repeat(update));

        self.send_updates(updates).await;
    }

    /// Handle follower timeout.
    async fn handle_follower_timeout(&mut self) {
        self.app_state.update_state(State::Candidate);
        let nodes = self.config.other_nodes();

        self.send_voting(nodes).await;
    }

    /// Send updates to all given nodes.
    async fn send_updates<'a, I: Iterator<Item=(&'a NodeConfig, UpdateRequest)>>(&self, updates: I) {
        let client = self.client.clone();
        let event_sender = self.event_sender.clone();

        let updates: Vec<_> = updates.map(|(node, request)| {
            (node.clone(), request)
        }).collect();

        // run in parallel
        tokio::spawn(async move {
            // create requests for all nodes
            let requests = updates
                .into_iter()
                .map(|(node, request)|
                    client.update(node, request)
                );

            // join requests and wait for all to complete
            let responses = join_all(requests).await;

            // send result to app queue
            if event_sender.send(AppEvent::UpdateResult(UpdateResult::new(responses))).await.is_err() {
                unimplemented!()
            }
        });
    }

    /// Send voting to all given nodes.
    async fn send_voting<'a, I: Iterator<Item=&'a NodeConfig>>(&self, nodes: I) {
        let client = self.client.clone();
        let event_sender = self.event_sender.clone();
        let term = self.app_state.term();

        let nodes: Vec<_> = nodes.cloned().collect();

        // run in parallel
        tokio::spawn(async move {
            let vote_request = VoteRequest::new(term);

            // create requests for all nodes
            let requests = nodes
                .into_iter()
                .map(|node|
                    client.vote(node, vote_request.clone())
                );

            // join requests and wait for all to complete
            let responses = join_all(requests).await;

            // send result to app queue
            if event_sender.send(AppEvent::VotingResult(VotingResult::new(responses))).await.is_err() {
                unimplemented!()
            }
        });
    }

    /// Run node colorization.
    async fn run_colorization(&mut self) {
        let count = self.app_state.followers().len() + 1;
        let mut greens = count / 3 + ((count % 3 != 0) as usize);
        greens -= 1;

        let mut updates = Vec::new();

        for follower in self.app_state.followers()[0..greens].iter() {
            let node = self.config.node(follower);
            let request = UpdateRequest::color(self.app_state.term(), ColorUpdate::Green);
            updates.push((node, request));
        }

        for follower in self.app_state.followers()[greens..].iter() {
            let node = self.config.node(follower);
            let request = UpdateRequest::color(self.app_state.term(), ColorUpdate::Red);
            updates.push((node, request));
        }

        self.send_updates(updates.into_iter()).await;
        self.app_state.update_color(Color::Green);
    }
}

/// An app queue sender for interaction with the app from other threads.
#[derive(Clone)]
pub struct AppQueueSender {
    sender: mpsc::Sender<AppEvent>,
}

impl AppQueueSender {
    /// Send a vote event to the app queue and wait for response.
    pub async fn vote(&self, vote_request: VoteRequest) -> VoteResponse {
        let (sender, receiver) = oneshot::channel();

        match self.sender.send(AppEvent::Vote(vote_request, sender)).await {
            Ok(_) => receiver.await.unwrap(),
            Err(_) => unimplemented!()
        }
    }

    /// Send an update event to the app queue and wait for response.
    pub async fn update(&self, update_request: UpdateRequest) -> UpdateResponse {
        let (sender, receiver) = oneshot::channel();

        match self.sender.send(AppEvent::Update(update_request, sender)).await {
            Ok(_) => receiver.await.unwrap(),
            Err(_) => unimplemented!()
        }
    }

    /// Send an check event to the app queue and wait for response.
    pub async fn check(&self) {
        let (sender, receiver) = oneshot::channel();

        match self.sender.send(AppEvent::Check(sender)).await {
            Ok(_) => receiver.await.unwrap(),
            Err(_) => unimplemented!()
        }
    }
}