//! RAFT protocol timeout values. All values are in milliseconds (ms)


/// Timeout for HTTP requests.
pub const REQUEST_TIMEOUT: u64 = 150;

/// Timeout until start election - lower bound.
pub const ELECTION_TIMEOUT_MIN: u64 = 1000;

/// Timeout until start election - upper bound.
pub const ELECTION_TIMEOUT_MAX: u64 = 3000;

/// Timeout until next message sent from leader.
pub const HEARTBEAT_TIMEOUT: u64 = 200;