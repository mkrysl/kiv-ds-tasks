# Task 3 - Implementation of the distributed MQTT broker following the architecture design from Task 3

Objective of this task is to implement a system of distributed MQTT broker using Apache ZooKeeper,
following the architecture design which was proposed in the [Task 3](../task3).

## Deployment architecture

The overall architecture can be seen on the following diagram.
Each node is an independent Docker container.

![eq1](img/deploy_diagram.png)

### ZooKeeper node

A single zookeeper node in larger ZooKeeper ensemble.


### Broker node

An MQTT broker with sidecar dispatcher for registering the MQTT broker within the
ZooKeeper ensemble and for monitoring other brokers.

It runs 2 separate programs:
  - **Mosquitto MQTT Broker**
  - **Dispatcher**
    - Registers the broker node to the ZooKeeper under `/cluster/<cluster_name>`
    - Listens to all other nodes registrations/unregistrations
    - Listens to all messages from all topics = `#` on the MQTT broker
      - Attaches a 'forwarded' tag (to prevent receiving and sending the same message back) and sends the messages to all other MQTT brokers
    - If the paired MQTT broker dies, dies too (and takes down the whole docker container)

The message forwarding between nodes is implemented using the HTTP protocol.
For this purpose, the broker provides an endpoint `POST /message` for receiving forwarded messages.

### Broker Client node
- Knows IPs of all the ZooKeeper nodes
- Chooses a ZooKeeper node to connect to
- Gets all available broker nodes
- Chooses one of broker nodes
- If the broker dies, chooses other one

The business logic of the client is very simple:
  - Sends a numbered greeting messages in format "`Greeting #{G} from client #{C}, my broker is #{B}.`",
  where `G` is a serial number of the greeting, `C` is client ID and `B` is client's current broker ID
  - Receives greeting messages from all other nodes and prints them
  
Messages are a JSON file and consists of the client ID (to prevent receiving own messages) and the message itself. 

## ZooKeeper namespace

The namespace of the ZooKeeper values is on the following diagram. The broker nodes information
is stored as JSON.

![eq1](img/namespace_diagram.png)

## Health checks

All nodes Docker containers implements a simple health checks.

- **ZooKeeper node**: checking client connection port using netcat
- **Broker node**: checking response on HTTP endpoint `GET /health`
- **Client node**: checking response on HTTP endpoint `GET /health`
