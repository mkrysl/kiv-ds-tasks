import json
import logging
import os
import threading

import flask
import requests
from paho.mqtt.client import Client as MqttClient
from kazoo.exceptions import NoNodeError
from kazoo.client import KazooClient

BROKER_CLUSTER_NODE = "cluster"
BROKER_NODE = "broker"

FORWARDED_TAG = "_forwarded"
MESSAGE_FORWARD_TIMEOUT = 1  # seconds

logger = logging.getLogger(__name__)


class Dispatcher:
    """
    MQTT messages repeater.
    """

    def __init__(self, broker_id, broker_ip, zookeeper_ensemble, broker_cluster):
        self.broker_id = broker_id
        self.broker_ip = broker_ip
        self.healthy = True

        self.brokers = []

        self.zookeeper_ensemble = zookeeper_ensemble
        self.broker_cluster = broker_cluster

        self.zookeeper_client = None
        self.broker_client = None

        self.lock = threading.Lock()
        self.condition = threading.Condition()
        self.stop = False

        self.flask_app = flask.Flask(__name__)

        @self.flask_app.get("/health")
        def get_health():
            return self.route_get_health()

        @self.flask_app.post("/message")
        def post_message():
            return self.route_post_message()

    def route_get_health(self):
        """
        Get health flag value and set it to false.
        """
        return "OK"

    def route_post_message(self):
        """
        Handles a HTTP received message forwarded from the the other broker.
        """

        forwarded_message = flask.request.json

        topic = forwarded_message["topic"]
        message = forwarded_message["message"]

        # add forwarded tag
        message[FORWARDED_TAG] = True

        logger.info(f"Sending forwarded message: {message['greeting']}.")

        self.lock.acquire()

        # forward the message to the local broker
        self.broker_client.publish(topic, json.dumps(message).encode("utf-8"))

        self.lock.release()
        return '', 204

    def on_broker_connect(self, client, userdata, flags, rc):
        """
        Handles connection to the local MQTT broker.
        """

        if rc == 0:
            self.broker_client.subscribe("#")
            logger.info("Connected to the local MQTT broker.")
        else:
            logger.error("Connecting to the local MQTT broker failed.")
            self.condition.acquire()
            self.condition.notify_all()
            self.condition.release()

    def on_broker_disconnect(self, client, userdata, rc):
        """
        Handles disconnection from the local MQTT broker.
        """

        logger.error("Connection to the local MQTT broker has been lost.")
        self.condition.acquire()
        self.condition.notify_all()
        self.condition.release()

    def on_broker_message(self, client, userdata, msg):
        """
        Handles a received message from the local MQTT broker and forwards it to other brokers.
        """
        topic = msg.topic
        message = json.loads(msg.payload.decode("utf-8"))

        if FORWARDED_TAG in message:
            return

        forwarded_message = {
            "topic": topic,
            "message": message
        }

        self.lock.acquire()
        brokers = [b for b in self.brokers]
        self.lock.release()

        logger.info(f"Forwarding a message: {message['greeting']}.")

        # forward the message to all other brokers
        for broker in brokers:
            try:
                requests.post(f'http://{broker["ip"]}/message', json=forwarded_message, timeout=MESSAGE_FORWARD_TIMEOUT)
            except Exception:
                pass

    def connect_to_zookeeper(self):
        """
        Connect to the zookeeper ensemble.
        """
        logger.info(f"Connecting to the zookeeper ensemble: [{self.zookeeper_ensemble}].")

        self.zookeeper_client = KazooClient(hosts=self.zookeeper_ensemble)
        self.zookeeper_client.start()

        self.register_broker()

        @self.zookeeper_client.ChildrenWatch(f"/{BROKER_CLUSTER_NODE}/{self.broker_cluster}")
        def watch_children(children):
            self.update_brokers(children)

    def connect_to_broker(self):
        """
        Connect to the local MQTT broker.
        """
        logger.info("Connecting to the local MQTT broker.")

        self.broker_client = MqttClient()
        self.broker_client.on_connect = self.on_broker_connect
        self.broker_client.on_message = self.on_broker_message
        self.broker_client.on_disconnect = self.on_broker_disconnect
        self.broker_client.connect(self.broker_ip)

        self.broker_client.loop_start()

    def register_broker(self):
        """
        Register the local broker in the zookeeper.
        """

        logger.info("Registering to the local MQTT broker to the zookeeper."
                     )
        broker_info = {
            "id": self.broker_id,
            "ip": self.broker_ip
        }

        self.zookeeper_client.create(f"/{BROKER_CLUSTER_NODE}/{self.broker_cluster}/{BROKER_NODE}",
                                     makepath=True,
                                     ephemeral=True,
                                     sequence=True,
                                     value=json.dumps(broker_info).encode("utf-8"))

    def update_brokers(self, brokers):
        """
        Update list available brokers.
        """
        self.lock.acquire()
        logger.info(f"Updating list of available brokers.")

        self.brokers = []
        for broker in brokers:
            try:
                data, stat = self.zookeeper_client.get(f"/{BROKER_CLUSTER_NODE}/{self.broker_cluster}/{broker}")
                broker_info = json.loads(data.decode("utf-8"))

                if broker_info["id"] != self.broker_id:
                    self.brokers.append(broker_info)

            except NoNodeError:
                pass

        logger.info(f"Other available brokers: {self.brokers}")
        self.lock.release()

    def loop(self):
        """
        The main loop.

        1) Runs MQTT client for listening messages
        2) Runs HTTP endpoints for forwarded messages
        3) Registers broker and get updates from the zookeeper
        """

        def run_flask():
            self.flask_app.run(host="0.0.0.0", port=80, debug=False)

        flask_thread = threading.Thread(target=run_flask, daemon=False)
        flask_thread.start()

        self.connect_to_broker()
        self.connect_to_zookeeper()

        self.broker_client.loop_start()

        self.condition.acquire()
        self.condition.wait_for(lambda: self.stop)
        self.condition.release()


def main():
    # get env variables
    zoo_ensemble = os.environ['ZOO_ENSEMBLE']
    broker_cluster = os.environ['BROKER_CLUSTER']
    broker_id = int(os.environ['BROKER_ID'])
    broker_ip = os.environ['BROKER_IP']

    # setup logging
    logging.basicConfig(level=None, format='[%(levelname)s]: %(message)s')
    logger.setLevel(logging.INFO)

    dispatcher = Dispatcher(broker_id, broker_ip, zoo_ensemble, broker_cluster)

    # start dispatcher
    dispatcher.loop()


if __name__ == "__main__":
    main()
