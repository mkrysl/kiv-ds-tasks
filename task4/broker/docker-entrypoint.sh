#!/bin/sh

set -e

/usr/sbin/mosquitto -d

# wait for mosquitto to start
sleep 10

python3 -u /opt/broker/dispatcher.py
