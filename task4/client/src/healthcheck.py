import requests

r = requests.get("http://localhost/health")

if r.ok and r.text == "OK":
    exit(0)
else:
    exit(1)