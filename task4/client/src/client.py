import json
import logging
import os
import random
import threading
import time

import flask
from paho.mqtt.client import Client as MqttClient
from kazoo.client import KazooClient
from kazoo.exceptions import NoNodeError

logger = logging.getLogger(__name__)

TOPIC = "greetings"
BROKER_CLUSTER_NODE = "cluster"
BROKER_REFRESH_DELAY = 3  # seconds
SEND_INTERVAL_MIN = 5  # seconds
SEND_INTERVAL_MAX = 30  # seconds


class Client:
    """
    The MQTT and Zookeeper joking client.
    """

    def __init__(self, client_id, zookeeper_ensemble, broker_cluster):
        self.client_id = client_id
        self.healthy = True
        self.broker_online = False
        self.broker_id = None
        self.greeting_id = 1

        self.zookeeper_ensemble = zookeeper_ensemble
        self.broker_cluster = broker_cluster

        self.zookeeper_client = None
        self.broker_client = None

        self.condition = threading.Condition()
        self.health_lock = threading.Lock()

        # setup flask for health check
        self.flask_app = flask.Flask(__name__)

        @self.flask_app.get("/health")
        def hello():
            return self.route_get_health()

    def route_get_health(self):
        """
        Get health flag value and set it to false.
        """
        self.health_lock.acquire()
        if self.healthy:
            response = "OK"
        else:
            response = "BAD"

        self.healthy = False
        self.health_lock.release()

        return response

    def set_health(self):
        """
        Set health flag value to true.
        """
        self.health_lock.acquire()
        self.healthy = True
        self.health_lock.release()

    def on_broker_connect(self, client, userdata, flags, rc):
        """
        Handles connection to an MQTT broker.
        """

        if rc == 0:
            logger.info("Connected to MQTT broker.")
            self.broker_client.subscribe(TOPIC)
        else:
            self.condition.acquire()

            logger.warning("Connecting to MQTT broker failed.")
            self.broker_client = None

            self.condition.release()

    def on_broker_disconnect(self, client, userdata, rc):
        """
        Handles disconnection from an MQTT broker.
        """

        self.condition.acquire()

        logger.warning("Connection to MQTT broker has been lost.")
        self.broker_online = False

        self.condition.notify_all()
        self.condition.release()

    def on_message(self, client, userdata, msg):
        """
        Handles received message.
        """

        if msg.topic == TOPIC:
            # greeting received

            message = json.loads(msg.payload.decode('utf-8'))

            if message["client_id"] == self.client_id:
                # received own message
                return

            print(f'<--- {message["greeting"]}')

    def connect_to_broker(self):
        """
        Connect to a random MQTT broker.
        """

        logger.info("Connecting to an MQTT broker.")

        if self.broker_client is not None:
            self.broker_client.loop_stop()

        while True:
            self.set_health()
            logger.warning("Choosing an MQTT broker.")

            # get the list of available brokers
            brokers = self.zookeeper_client.get_children(f"/cluster/{self.broker_cluster}")

            if len(brokers) <= 0:
                logger.warning(f"No MQTT brokers available. Retry in {BROKER_REFRESH_DELAY} seconds.")
                time.sleep(BROKER_REFRESH_DELAY)
                continue

            # choose a random broker
            broker = random.choice(brokers)

            # fetch broker info
            try:
                data, stat = self.zookeeper_client.get(f"/{BROKER_CLUSTER_NODE}/{self.broker_cluster}/{broker}")
                broker_info = json.loads(data.decode("utf-8"))
                broker_id = broker_info["id"]
                broker_ip = broker_info["ip"]

                logger.info(f"Selected broker: {broker_id}, with IP: {broker_ip}.")

            except NoNodeError:
                logger.warning(
                    f"Error while fetching info about the MQTT broker. Retry broker selection in {BROKER_REFRESH_DELAY} seconds.")
                time.sleep(BROKER_REFRESH_DELAY)
                continue

            try:
                # connect to the selected MQTT broker
                broker_client = MqttClient()
                broker_client.on_connect = self.on_broker_connect
                broker_client.on_message = self.on_message
                broker_client.on_disconnect = self.on_broker_disconnect
                broker_client.connect(broker_ip)

                self.broker_client = broker_client
                self.broker_client.loop_start()
                self.broker_online = True
                self.broker_id = broker_id
                break

            except Exception:
                # an error occurred while connecting
                logger.warning(
                    f"Error while connecting to the MQTT broker. Retry broker selection in {BROKER_REFRESH_DELAY} seconds.")
                time.sleep(BROKER_REFRESH_DELAY)
                continue

    def connect_to_zookeeper(self):
        """
        Connect to the zookeeper ensemble.
        """
        logger.info(f"Connecting to the zookeeper ensemble: [{self.zookeeper_ensemble}].")

        self.zookeeper_client = KazooClient(hosts=self.zookeeper_ensemble)
        self.zookeeper_client.start()

    def loop(self):
        """
        The main loop.

        1) Query available brokers from ZooKeeper ensemble
        2) Connect to the selected MQTT broker
        3) Send greetings in random intervals and listen to other clients greetings.

        If the currently selected broker fails, the client automatically reconnects to another available broker.
        """

        self.connect_to_zookeeper()

        def run_flask():
            self.flask_app.run(host="0.0.0.0", port=80)

        flask_thread = threading.Thread(target=run_flask)
        flask_thread.start()

        while True:
            # connect to a broker
            self.connect_to_broker()

            next_message_time = time.time()

            # run jokes publishing loop
            self.condition.acquire()
            while self.broker_online:
                self.set_health()

                next_message_time_remaining = next_message_time - time.time()

                if next_message_time_remaining <= 0:
                    # publish message

                    next_message_time += random.randint(SEND_INTERVAL_MIN, SEND_INTERVAL_MAX)

                    message = {
                        "client_id": self.client_id,
                        "greeting": f"Greeting #{self.greeting_id} from client #{self.client_id}, my broker is #{self.broker_id}."
                    }
                    self.greeting_id += 1

                    logger.info(f"Sending a message.")
                    self.broker_client.publish(TOPIC, json.dumps(message).encode("utf-8"))

                # wait max 1 second
                self.condition.wait_for(lambda: not self.broker_online, 1)

            self.condition.release()


def main():
    # get env variables
    zoo_ensemble = os.environ['ZOO_ENSEMBLE']
    broker_cluster = os.environ['BROKER_CLUSTER']
    client_id = int(os.environ['CLIENT_ID'])

    # setup logging
    logging.basicConfig(level=None, format='[%(levelname)s]: %(message)s')
    logger.setLevel(logging.INFO)

    # setup random
    random.seed(client_id)

    client = Client(client_id, zoo_ensemble, broker_cluster)

    # start client
    client.loop()


if __name__ == "__main__":
    main()
